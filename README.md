# CI + CD Pipeline Implementation Project - Krupp Healthcare

## Project Purpose

The purpose of this project is to illustrate the increased efficiency that comes from implementing the development process of Continuous Integration and Continuous Deployment (CI / CD).  The context of the project is within the “in-house” development team at a major integrated managed care consortium (IMCC). The IMCC is referred to with the pseudonym, “Krupp Healthcare” or simply, “Krupp.”

## Krupp Healthcare

Krupp requested Caltech CTME to help with their software development process. Specifically, Krupp wanted a new process with iterative deployments, automated testing, continuous integration, and continuous deployment. Krupp wanted implementation of the new process to result in faster feature releases, improved service quality and reduced costs for their corporate portal and front facing member website.   

## Project Organization

A detailed narrative of Caltech CTME’s development process recommendations is presented in the PDF file. The project is organized into three parts. First, Krupp’s legacy software development cycle and deficiencies in the cycle are analyzed.  Second, specific aspects and advantages of implementing the CI/CD methodology are outlined.   Third, an illustrative implementation of the CI/CD process is provided.  

## Uploaded Files

<b><i>sourc.main</i></b>

The source. main directory contains two sub-directories which hold the Java / HTML application used to illustrate the CI / CD process. The Java application is the “application server.”  The application server is a simple Java appliation that utilizes the Springboard Framework. The other directory holds the front-end web application where the application is a simple HTML landing page. 

<b><i>dockerfile</i></b>

The dockerfile lists commands needed to create a specific Docker image. The application is run as a container where the container is an active / running iteration of the Docker image.  

<b><i>jenkinsfile</i></b>

The jenkinsfile contains line-by-line commands for the pipeline build and is the embodiment of the CI/CD process. 

<b><i>pom.xml</i></b>

The pom.xml file is the dependency file for the Java application.  The dependencies are downloaded from the Maven repository then compiled and compressed - along with the Java application code - at build time. 

<b><i>script.groovy</i></b>

The script.groovy file contains logic extracted from the Jenkins file. During run time, code in the Jenkinsfile redirects pipeline execution to the Groovy file. The purpose of the Groovy file is to separate pipeline logic from the jenkinsfile. 

